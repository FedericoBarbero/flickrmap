//
//  MasterViewController.swift
//  FlickrMap
//
//  Created by Andrea Mauro on 24/11/15.
//  Copyright © 2015 Andrea Mauro. All rights reserved.
//

import UIKit
import MapKit


class MasterViewController: UIViewController, MKMapViewDelegate {

    var detailViewController: DetailViewController? = nil
    var objects = [AnyObject]()
    var kml: KMLParser!
    var actInd : UIActivityIndicatorView!
    var images: NSMutableArray!
    
    @IBOutlet weak var map: MKMapView!

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        let pin: MKAnnotationView = kml.viewForAnnotation(annotation)
        let rightButton: UIButton! = UIButton(type: UIButtonType.DetailDisclosure)
        rightButton.addTarget(self, action: "showDetail:", forControlEvents: UIControlEvents.TouchUpInside)
        
        rightButton.tag = images.count
        let url = kml.imageURLForAnnotation(annotation)
        self.images.addObject(url)
        pin.rightCalloutAccessoryView = rightButton
        return pin
    }
    
    func showDetail(sender: UIButton){
        print("OK! SHOW DETAIL CALLBACK")
        //start activity indicator view
        actInd  = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        actInd.backgroundColor = UIColor.grayColor()
        view.addSubview(actInd)
        actInd.startAnimating()
        
        self.detailViewController = storyboard!.instantiateViewControllerWithIdentifier("DetailViewController") as? DetailViewController
        //instantiate a bg thread
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)){
            let button = sender
            let url:NSURL! = self.images.objectAtIndex(button.tag) as! NSURL
            let data:NSData! = NSData(contentsOfURL: url)
            dispatch_async(dispatch_get_main_queue()){
                self.detailViewController!.imageURL = url //image.image = UIImage(data: data)
                self.detailViewController!.data = data
                self.detailViewController!.images = self.images
                self.actInd.stopAnimating()
                self.navigationController?.pushViewController(self.detailViewController!, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.images = NSMutableArray()
        
        let url = NSURL(string: "https://www.flickr.com/services/feeds/geo/fr?format=kml&page=1")
        self.kml = KMLParser .parseKMLAtURL(url)
        let annotations : NSArray = kml.points
        map.addAnnotations(annotations as! [MKAnnotation])
        map.visibleMapRect = kml.pointsRect()
        
    }

    override func viewWillAppear(animated: Bool) {
 //       self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

