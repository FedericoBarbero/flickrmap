//
//  DetailViewController.swift
//  FlickrMap
//
//  Created by Andrea Mauro on 24/11/15.
//  Copyright © 2015 Andrea Mauro. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController , UIScrollViewDelegate {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    var imageURL: NSURL!
    var data:NSData!
    var image: UIImageView!
    var actInd : UIActivityIndicatorView!
    var first : Bool = true
    var images: NSMutableArray!
    

    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }
    
    func setZoomScale() {
        let imageViewSize = image.bounds.size
        let scrollViewSize = scrollView.bounds.size
        let widthScale = scrollViewSize.width / imageViewSize.width
        let heightScale = scrollViewSize.height / imageViewSize.height
        
        scrollView.minimumZoomScale = min(widthScale, heightScale)
        scrollView.maximumZoomScale = 1.5
        scrollView.zoomScale = 1.0
    }

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = self.detailItem {
            if let label = self.detailDescriptionLabel {
                label.text = detail.description
            }
        }
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return image
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
        
        if !first {
            self.image.removeFromSuperview()
        }
        
        self.image = UIImageView(image: UIImage(data: (self.data)!))
        image.center = scrollView.center
        self.image.contentMode = .ScaleToFill
        
        scrollView.backgroundColor = UIColor.blackColor()
        scrollView.contentSize = image.bounds.size
        scrollView.autoresizingMask = UIViewAutoresizing.FlexibleHeight
        scrollView.addSubview(image)
        scrollView.contentSize = self.image.image!.size
        
        if first {
            setupGestureRecognizer()
            setupSwipeGestureRecognizer()
        }
      
        setZoomScale()
        scrollView.delegate = self
        first = false
      
    }
    
    /*override func viewWillLayoutSubviews() {
        setZoomScale()
    }*/
    
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        let imageViewSize = image.frame.size
        let scrollViewSize = scrollView.bounds.size
        
        let verticalPadding = imageViewSize.height < scrollViewSize.height ? (scrollViewSize.height - imageViewSize.height) / 2 : 0
        let horizontalPadding = imageViewSize.width < scrollViewSize.width ? (scrollViewSize.width - imageViewSize.width) / 2 : 0
        view.center = scrollView.center
        scrollView.contentInset = UIEdgeInsets(top: verticalPadding, left: horizontalPadding, bottom: verticalPadding, right: horizontalPadding)
        
    }
    
    func setupSwipeGestureRecognizer(){
        let Rswipe = UISwipeGestureRecognizer(target: self, action: "swipeRight:")
        let Lswipe = UISwipeGestureRecognizer(target: self, action: "swipeLeft:")
        Rswipe.direction = UISwipeGestureRecognizerDirection.Right
        Lswipe.direction = UISwipeGestureRecognizerDirection.Left
        scrollView.addGestureRecognizer(Rswipe)
        scrollView.addGestureRecognizer(Lswipe)
    }
    
    func setupGestureRecognizer() {
        let doubleTap = UITapGestureRecognizer(target: self, action: "handleDoubleTap:")
        doubleTap.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(doubleTap)
    }
    
    func handleDoubleTap(recognizer: UITapGestureRecognizer) {
        
        if (scrollView.zoomScale > 1.0) {
            //setZoomScale()
            scrollView.setZoomScale(scrollView.minimumZoomScale, animated: true)
        } else {
            scrollView.setZoomScale(scrollView.maximumZoomScale, animated: true)
        }
    }
    
    func swipeLeft(recognizer: UISwipeGestureRecognizer){
        var i = 0
        for nurl in images {
            if imageURL == nurl as! NSURL {
                break
            }
            ++i
        }
        
        if i == images.count {
            return
        }
        
        //start activity indicator view
        actInd  = UIActivityIndicatorView(frame: CGRectMake(0,0, 50, 50)) as UIActivityIndicatorView
        actInd.center = self.view.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        actInd.backgroundColor = UIColor.grayColor()
        view.addSubview(actInd)
        actInd.startAnimating()
        
        
        //self.detailViewController = storyboard!.instantiateViewControllerWithIdentifier("DetailViewController") as? DetailViewController
        //instantiate a bg thread
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)){
            let url:NSURL! = self.images.objectAtIndex(i) as! NSURL
            let data:NSData! = NSData(contentsOfURL: url)
            dispatch_async(dispatch_get_main_queue()){
                self.imageURL = url //image.image = UIImage(data: data)
                self.data = data
                self.viewDidLoad()
                //self.actInd.stopAnimating()
            }
        }
    }
    
    func swipeRight(recognizer: UISwipeGestureRecognizer){
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

