//
//  MasterViewController.swift
//  FlickrMapSwift
//
//  Created by Federico Barbero on 24/11/15.
//  Copyright © 2015 eurecom. All rights reserved.
//

import UIKit
import MapKit

class MasterViewController: UIViewController {
    @IBOutlet weak var map: MKMapView!
    var detailViewController: DetailViewController? = nil
    var objects = [AnyObject]()
    var kml: KMLParser!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        /*
        self.navigationItem.leftBarButtonItem = self.editButtonItem()

        let addButton = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: "insertNewObject:")
        self.navigationItem.rightBarButtonItem = addButton
        if let split = self.splitViewController {
            let controllers = split.viewControllers
            self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }*/
        
        let url = NSURL(string: "https://www.flickr.com/services/feeds/geo/it?format=KML&page=1")
        self.kml = KMLParser.parseKMLAtURL(url)
        print("POINTS: ", kml.points.count)
        let annotations: NSArray = kml.points
        
        map.addAnnotations(annotations as! [MKAnnotation])
        map.visibleMapRect = kml.pointsRect()
    }

    override func viewWillAppear(animated: Bool) {
        //self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    func insertNewObject(sender: AnyObject) {
        //objects.insert(NSDate(), atIndex: 0)
        //let indexPath = NSIndexPath(forRow: 0, inSection: 0)
        //self.tableView.insertRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
    }*/

    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            /*if let indexPath = self.tableView.indexPathForSelectedRow {
                let object = objects[indexPath.row] as! NSDate
                let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
                controller.navigationItem.leftItemsSupplementBackButton = true
            }*/
        }
    }
}

